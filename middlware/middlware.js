const jwt = require('jsonwebtoken');

const registrado = (req, res, next) => {
    const { token } = req.body;
    try {
      const decoded = jwt.verify(token.token, 'secret');
      req.usuario = decoded;
      next();
    } catch(err) {
      return res.status(400).json({err});
    }
};

module.exports = { registrado };


