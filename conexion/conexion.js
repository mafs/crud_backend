const Sequelize = require('sequelize');

const sequelize = new Sequelize('inicio', 'user', 'password', {
    host: 'localhost',
    dialect: 'mysql' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
}, { pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
    // timestamps: false
  }});
sequelize
    .authenticate()
    .then(() => {
      console.log('Conexion se ha establecido');
    })
    .catch(err => {
      console.error('Ocurrio un error', err);
});

const db = {};

db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
