const Sequelize = require('sequelize');
const db = require('../conexion/conexion');

const User = db.sequelize.define('usuarios', 
{
    idUsuario: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nombre: {
        type: Sequelize.STRING
    },
    password:{
        type: Sequelize.STRING
    }
}, 
{
    timestamps: false
}
);

module.exports = User;