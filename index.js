const express = require('express');
const app = express();
const morgan = require('morgan');
// const bodyParser = require('body-parser');
// const cors = require('cors');


require('./conexion/conexion');

app.set('port', process.env.PORT || 3000);

app.use(function(req, res, next) {
    console.log('entro al middlware');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
app.use(express.json({limit:'50mb'}));
app.use(express.urlencoded({extended:true, limit:'50mb', parameterLimit: 1000000}));

app.use(morgan('dev'));
app.get('/', (req, res) => {
    const data = [{"lugar": "cuarto", "estado" : true},{"lugar": "cocina", "estado" : false}];
    res.json(data);
});

const rutasUsuario = require('./rutas/usuario');
app.use('/user', rutasUsuario);

app.listen(app.get('port'), () => console.log(`server on port ${app.get('port')} `));