const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const { registrado } = require('../middlware/middlware');

// todos los usuario
router.get('/', (req, res) => {
    
    User.findAll()
    // User.findOne({ where: {idUsuario: 1}})
    // User.findByPk(2)
    .then(data => res.json({data}))
        .catch(e => console.log(e));
});

// registrarte
router.post('/', async (req, res) => {
    // console.log(req.usuario);
    const { nombre, password } = req.body;
    try {
        const hash = await bcrypt.hash(password, 10);
        const datos = await User.create({nombre, password: hash});
        return res.json({datos});
    } catch (err) {
        return res.status(400).json({err});
    } 
});

// actualizar
router.put('/update/:id', registrado ,async (req, res) => {
    const id = +req.params.id;
    
    const { nombre } = req.body;
    try {
        const datos = await User.update({nombre},{ where: {idUsuario: id}});
        res.json({datos});
    } catch (err) {
        res.status(400).json({err});
    }
});
// eliminar
router.delete('/delete/:id', async (req, res) => {
    const id = +req.params.id;
    // res.json({id});
    try {
        const datos = await User.destroy({where: {idUsuario: id}});
        res.json({datos});
    } catch (err) {
        res.status(400).json({err});
    }
});

router.post('/ingresar', (req, res) => {
    
    const { usuario, password } = req.body;
    User.findOne({ where: {nombre:usuario}})
        .then(async (user) => {
            if(!user){
                return res.status(402).json({ok:false, msj: 'el usuario es incorrecto'});
            } else {
                try {
                    const match = await bcrypt.compare(password, user.password);
                    if(match) {
                        jwt.sign({user}, 'secret', (err, token) => {
                            if(err) return res.status(400).json({ok:false});
                            res.json({token});
                        });
                    }
                } catch (err) {
                    return res.status(400).json({err});
                }
            }
        })
        .catch(err => res.status(402).json({err}));
});

// registrarte
router.post('/registrarte', async (req, res) => {
    // console.log(req.body);
    const { usuario, passwordGroup } = req.body.datos;
    console.log(usuario, passwordGroup);
    if(!usuario || !passwordGroup.password) return res.status(400).json({err: true, msj: 'el usuario o el password viene en blanco'});
    
    try {
        const hash = await bcrypt.hash(passwordGroup.password, 10);
        const datos = await User.create({nombre: usuario, password: hash});
        return res.json({datos, msj: 'se guardo el usario correctamente'});
    } catch (err) {
        return res.status(400).json({err});
    } 
});


router.post('/imagen/:id/usuario', (req, res) => {
    res.json({ok: true});
});


// module.exports = ;
module.exports = router;




// if (fs.existsSync(`img`)) {
        // fs.unlink(`/img/`, (err) => {
        //     if (err) throw err;
        //     return res.end('la imagen fue borrada');
        //   });
    //     return res.end('El archivo existe');
    //   } else {
    //       return res.end('no existe el archivo');
    //   }
